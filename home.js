

// let homeMore = document.querySelector('#home-more');
let objLoad = document.querySelectorAll('.animated');
window.addEventListener('scroll',(event)=>{
    objLoad.forEach((elLoad)=>{
        let offsetTop = Number(elLoad.offsetTop);
        if(offsetTop - window.scrollY < window.innerHeight - 100){
          elLoad.classList.add('active');
        }
    });
})

$(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() > 300) $(".back-to-top").fadeIn();
    else $(".back-to-top").fadeOut();
  });
  $(".back-to-top").click(function () {
    $("html, body").animate({ scrollTop: 0 }, 0);
  });
})